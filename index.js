//Get SearchBar value
let userInput


let itemEndNumber=20

//LoadMore button function
const loadMore=()=>{
  if(itemEndNumber<=180){
    itemEndNumber+=20
  }
  if(itemEndNumber>200) itemEndNumber=200

  console.log(itemEndNumber)
  return getAPIData(userInput, itemEndNumber)
}



const search= ()=>{
  userInput= document.getElementById('InputValue').value
  document.getElementById('result').innerHTML=''
  if(userInput.length===0){
    document.getElementById('result').innerHTML=`<div class="notification">Please enter keyword !</div>`
    return
  }else{
    document.getElementById('loadmore').style.display='initial'
    return getAPIData(userInput, itemEndNumber)
  }
}


const reset = ()=>{
  document.getElementById('InputValue').value =''
  document.getElementById('result').innerHTML=''
  document.getElementById('loadmore').style.display='none'
}

//Get API data
const getAPIData = (name, number)=>{
  return fetch(`https://itunes.apple.com/search?term=${name}&media=music&entity=album&attribute=artistTerm&limit=200`)
  .then(data=> data.json())
  .then(res=>{
    //console.log(res.results[0]) Take a look at the first item
    res.results.slice(0, number).map(item=>{
      document.getElementById('result').innerHTML+=
      `<div class="item">
         <img src=${item.artworkUrl100} width='100%' />
         <div>${item.artistName}</div>
      </div>`

    })
  })
}

const input = document.getElementById("InputValue")
input.addEventListener("keypress", function onEvent(event) {
  if (event.key === "Enter") {
      document.getElementById("search").click();
  }
});

